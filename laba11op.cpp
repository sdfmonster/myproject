﻿#include "pch.h"
#include <iostream>
#include <windows.h>
#include <fstream>
#include <cstring>
using namespace std;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	const int S = 256;
	char a[S] = "";
	char b[S] = "";
	int k = 0, cnt = 0;
	cout << "Введите строку 1:" << endl;
	cin.getline(a, S);
	cout << "Строка: [" << a << "] \n";
	cout << "Введите строку 2:" << endl;
	cin.getline(b, S);
	int const la = strlen(a);
	int const lb = strlen(b);
	cout << "Строка: [" << b << "] \n";
	for (int i = 0; i < la; i++) {
		char tmp[S] = "";
		k = 0;
		if (a[i] == b[0]) {
			int j = i;
			while (a[j] == b[k]) {
				tmp[k] = a[j];
				j++; k++;
			}
			if (!(strcmp(b, tmp)))cnt++;
		}
	}
	cout << "Количество вхождений: " << cnt << endl;
	system("pause");
	return 0;
}